import { useState, createRef, useRef } from "react";
import { NumberSize, Resizable } from "re-resizable";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowsUpDownLeftRight } from "@fortawesome/free-solid-svg-icons";
import { Direction } from "re-resizable/lib/resizer";

import { reorder, getListStyle, getItemStyle } from "./utils";

import "./App.css";

function App() {
  const style = {
    display: "flex",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    border: "solid 1px #ddd",
  };

  const [width, setWidth] = useState<{ id: string; color: string }[]>([
    {
      id: "85",
      color: "red",
    },
    {
      id: "86",
      color: "blue",
    },
    {
      id: "87",
      color: "green",
    },
    {
      id: "88",
      color: "yellow",
    },
    {
      id: "89",
      color: "orange",
    },
  ]);

  const resizeRef = useRef<any[]>([]);

  const onDragEnd = (result: any) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(width, result.source.index, result.destination.index);

    setWidth(
      items as {
        id: string;
        width: string;
        color: string;
      }[]
    );
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable" direction="horizontal">
        {(provided, snapshot) => (
          <div
            className="App"
            ref={provided.innerRef}
            style={getListStyle(snapshot.isDraggingOver)}
            {...provided.droppableProps}
          >
            {width.map((val, index) => {
              return (
                <Draggable key={val.id} draggableId={val.id} index={index}>
                  {(providedDrag, snapshotDrag) => {
                    return (
                      <Resizable
                        ref={(c) => resizeRef.current.push(c)}
                        bounds="parent"
                        key={`${val.id}`}
                        {...providedDrag.draggableProps}
                        style={{
                          ...getItemStyle(
                            snapshotDrag.isDragging,
                            providedDrag.draggableProps.style
                          ),
                          background: val.color,
                        }}
                        enable={{
                          top: false,
                          bottom: false,
                          right: true,
                          left: true,
                        }}
                        as="div"
                        defaultSize={{
                          width: `${100 / width.length}%`,
                          height: 35,
                        }}
                        maxHeight="50px"
                        maxWidth="90%"
                        minWidth="10%"
                        onResizeStop={(
                          event: MouseEvent | TouchEvent,
                          direction: Direction,
                          elementRef: HTMLElement,
                          delta: NumberSize
                        ) => {
                          const clientWidth = elementRef.clientWidth;
                          const parentSize =
                            resizeRef?.current[index].getParentSize().width;
                          const widthInPercent = `${
                            (clientWidth * 100) / parentSize
                          }%`;
                          console.log({
                            event,
                            direction,
                            clientWidth,
                            delta,
                            size: resizeRef?.current[index].size,
                            parentSize,
                            widthInPercent,
                          });
                        }}
                      >
                        <div style={{ ...style }} ref={providedDrag.innerRef}>
                          {`00${val.id}`}
                          <span
                            className="drag-icon-wrapper"
                            {...providedDrag.dragHandleProps}
                          >
                            <FontAwesomeIcon icon={faArrowsUpDownLeftRight} />
                          </span>
                        </div>
                      </Resizable>
                    );
                  }}
                </Draggable>
              );
            })}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default App;
